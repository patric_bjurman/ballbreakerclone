export default class Paddle {
  constructor(game) {
    this.gameWidth = game.gameWidth;
    this.gameHeight = game.gameHeight;

    this.width = 150;
    this.height = 30;

    this.maxSpeed = 7;
    this.speed = 0;

    this.position = {
      x: this.gameWidth / 2 - this.width / 2,
      y: this.gameHeight - this.height - 10,
    };
  }

  draw(ctx) {
    ctx.fillStyle = "#0ff";
    ctx.fillRect(this.position.x, this.position.y, this.width, this.height);
  }

  update(deltaTime) {
    this.move();
    this.checkCollision();
  }

  move() {
    this.position.x += this.speed;
  }

  moveLeft() {
    this.speed = -this.maxSpeed;
  }

  moveRight() {
    this.speed = this.maxSpeed;
  }

  checkCollision() {
    if (this.position.x > this.gameWidth) this.position.x = 0 - this.width;
    else if (this.position.x + this.width < 0) this.position.x = this.gameWidth;
  }

  stop() {
    this.speed = 0;
  }
}
