import Game from "/src/game.js";

let canvas = document.getElementById("gameScreen");
let ctx = canvas.getContext("2d");

// GAME_SIZE
const GAME_WIDTH = 800;
const GAME_HEIGHT = 600;

let GAME = new Game(GAME_WIDTH, GAME_HEIGHT);

// GAME_LOOP
let lastTime = 0;
function gameLoop(timestamp) {
  let deltaTime = timestamp - lastTime;
  lastTime = timestamp;
  ctx.clearRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
  GAME.update(deltaTime);
  GAME.draw(ctx);
  requestAnimationFrame(gameLoop);
}

requestAnimationFrame(gameLoop);
