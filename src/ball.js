import { detectCollision } from "/src/collisionDetection.js";

export default class Ball {
  constructor(game) {
    this.image = document.getElementById("ball_img");
    this.game = game;

    this.gameWidth = game.gameWidth;
    this.gameHeight = game.gameHeight;

    this.size = 28;
    this.reset();
  }

  reset() {
    this.position = { x: 300, y: 250 };
    this.speed = { x: 4, y: 4 };
  }

  draw(ctx) {
    ctx.drawImage(
      this.image,
      this.position.x,
      this.position.y,
      this.size,
      this.size
    );
  }

  update(deltaTime) {
    this.position.x += this.speed.x;
    this.position.y += this.speed.y;
    this.checkCollision();
  }

  checkCollision() {
    this.checkWallCollision();
    this.checkPaddleCollision();
  }

  checkWallCollision() {
    if (this.position.x + this.size > this.gameWidth || this.position.x < 0)
      this.speed.x = -this.speed.x;
    if (this.position.y < 0) this.speed.y = -this.speed.y;
    if (this.position.y + this.size > this.gameHeight) {
      this.game.lives--;
      this.reset();
    }
  }

  checkPaddleCollision() {
    if (detectCollision(this, this.game.paddle)) {
      this.speed.y = -this.speed.y;
      this.position.y = this.game.paddle.position.y - this.size;
    }
  }
}
