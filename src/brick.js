import { detectCollision } from "/src/collisionDetection.js";

export default class Brick {
  constructor(game, position) {
    this.image = document.getElementById("brick_img");

    this.game = game;
    this.gameWidth = game.gameWidth;
    this.gameHeight = game.gameHeight;

    this.width = 80;
    this.height = 40;
    this.position = position;

    this.markedForDeletion = false;
  }

  draw(ctx) {
    ctx.drawImage(
      this.image,
      this.position.x,
      this.position.y,
      this.width,
      this.height
    );
  }

  update(deltaTime) {
    this.checkCollision();
  }

  checkCollision() {
    this.checkBallCollision();
  }

  checkBallCollision() {
    if (detectCollision(this.game.ball, this)) {
      this.game.ball.speed.y = -this.game.ball.speed.y;
      this.markedForDeletion = true;
    }
  }
}
